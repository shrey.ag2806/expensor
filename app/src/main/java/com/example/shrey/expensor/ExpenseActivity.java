package com.example.shrey.expensor;

import android.app.DialogFragment;
import android.content.ContentUris;
import android.content.ContentValues;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shrey.expensor.Utils.CalendarUtil;
import com.example.shrey.expensor.Utils.DatePickerFragment;
import com.example.shrey.expensor.data.ExpenseContract;
import com.example.shrey.expensor.data.Expense_CProvider;
import com.example.shrey.expensor.data.QueryHelper;
import com.nmaltais.calcdialog.CalcDialog;

import java.math.BigDecimal;
import java.sql.Date;

import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_DATE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_NOTE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_PVALUE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_TITLE;

public class ExpenseActivity extends AppCompatActivity implements CalcDialog.CalcDialogCallback {


    private TextView priceChoose;
    private ImageView currency_img;

    private TextView dateEdt;
    private BigDecimal value;
    private FloatingActionButton expense_save;
    private EditText title_edt;
    private EditText note_edt;

    private Expense_CProvider provider;

    private CalendarUtil calendarUtil;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense);

        priceChoose = findViewById(R.id.price_choose_txtview);
        currency_img = findViewById(R.id.currency_icon);


        dateEdt = findViewById(R.id.date_textview);
        expense_save = findViewById(R.id.expense_save_btn);
        title_edt = findViewById(R.id.title_editText);
        note_edt = findViewById(R.id.note_editText);


        value = null;
        final CalcDialog calcDialog = new CalcDialog();
        priceChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                calcDialog.setValue(value);
                calcDialog.show(getSupportFragmentManager(), "calc_dialog");

            }
        });

        dateEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");

            }
        });

        expense_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertExpense();
            }
        });
    }

    //Insert Method----of expenses into database

    private void insertExpense() {
        String price_string = priceChoose.getText().toString().trim();
        String title_string = title_edt.getText().toString().trim();
        String note_string = note_edt.getText().toString().trim();
        String date_string = dateEdt.getText().toString().trim();

        if (date_string.equals("Today")){
            date_string =CalendarUtil.getDate();
        }

        // Creating Content Values object where column names are keys and above strings are values;
        if (!TextUtils.isEmpty(price_string) && !TextUtils.isEmpty(title_string)) {

            Double price = Double.parseDouble(price_string);

            Date d = new Date(CalendarUtil.getYearInt(date_string),
                   CalendarUtil.getMonthInt(date_string)
                    ,CalendarUtil.getDateFromString2(date_string));

            ContentValues values = new ContentValues();
            values.put(COLUMN_PVALUE, price);
            values.put(COLUMN_TITLE, title_string);
            values.put(COLUMN_NOTE, note_string);
            values.put(COLUMN_DATE,String.valueOf(d));

            //inserting into content provider
            Uri uri = getContentResolver().insert(ExpenseContract.ExpenseEntry.CONTENT_URI, values);
            long id = ContentUris.parseId(uri);


            QueryHelper.updateTheTotal(price, id,date_string,this);
            Toast.makeText(this, "Expense has been added", Toast.LENGTH_LONG).show();
            finish();

        } else {
            Toast.makeText(this, "Price and Title cannot be empty", Toast.LENGTH_LONG).show();
        }
    }


    //-------------DATE PICKER METHODS ------------
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }

    @Override
    public void onValueEntered(BigDecimal value) {
        priceChoose.setText(value.toPlainString());

    }

    //------------------------------------


}
