package com.example.shrey.expensor.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import static android.support.constraint.Constraints.TAG;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.TABLE_NAME;

public class Expense_CProvider extends ContentProvider {


    //Tag for log Messages;
    public static final String LOG_TAG = ContentProvider.class.getSimpleName();


    //TODO: define codes for Single exppense and expense table

    //Uri matcher code for whole expense table :
    private static final int EXPENSE = 100;

    //Uri matcher code for particular expense :
    private static final int EXPENSE_ID = 101;

    //Uri matcher code for whole total expense :
    private static final int M_TOTAL_EXPENSE = 200;

    //Uri matcher code for particular total expense :
    private static final int M_TOTAL_EXPENSE_ID = 201;

    /*
      A UriMatcher object to match a content URI to a corresponding code.
      The input passed into the constructor represents the code to return for the root URI.
      It's common to use NO_MATCH as the input for this case.

    */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);


    //We create static initializer . this is created first time anything is called from this class

    static {


        sUriMatcher.addURI(ExpenseContract.CONTENT_AUTHORITY, ExpenseContract.PATH_EXPENSE, EXPENSE);

        sUriMatcher.addURI(ExpenseContract.CONTENT_AUTHORITY, ExpenseContract.PATH_EXPENSE + "/#", EXPENSE_ID);

        // '#' is wildcard , it can be substituted for integer


        sUriMatcher.addURI(ExpenseContract.CONTENT_AUTHORITY, ExpenseContract.PATH_TOTAL_EXPENSE, M_TOTAL_EXPENSE);
        sUriMatcher.addURI(ExpenseContract.CONTENT_AUTHORITY, ExpenseContract.PATH_TOTAL_EXPENSE + "/#", M_TOTAL_EXPENSE_ID);
    }


    private ExpenseDbHelper mhelper;




    public boolean onCreate() {

        mhelper = new ExpenseDbHelper(getContext());

        return true;
    }
//-------------------------------------------------------------------------------------------------------------------------//

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteDatabase database = mhelper.getReadableDatabase();

        Cursor cursor;

        int match = sUriMatcher.match(uri);

        switch (match) {

            case EXPENSE:
                cursor = database.query(TABLE_NAME, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;

            case EXPENSE_ID:

                selection = ExpenseContract.ExpenseEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = database.query(TABLE_NAME, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;


            case M_TOTAL_EXPENSE:

                cursor = database.query(ExpenseContract.ExpenseEntry.TABLE2, projection, selection,
                        selectionArgs, null, null, sortOrder);

                Log.i(TAG, " query : sUriMatcher matched M_TOTAL_EXPENSE");
                break;


            case M_TOTAL_EXPENSE_ID:

                selection = ExpenseContract.ExpenseEntry._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };
                cursor = database.query(ExpenseContract.ExpenseEntry.TABLE2, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;

            default:
                throw new IllegalArgumentException("Cannot query Unknown URI:" + uri);

        }


        // Set notification URI on the Cursor,
        // so we know what content URI the Cursor was created for.
        // If the data at this URI changes, then we know we need to update the Cursor.
        cursor.setNotificationUri(getContext().getContentResolver(), uri);


        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {

        return null;
    }

    @Nullable

    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case EXPENSE:
                return insertExpense(uri, contentValues);

            case M_TOTAL_EXPENSE:
                return insert_monthlyExpense(uri, contentValues);

            default:
                throw new IllegalArgumentException("Insertion is not supported for " + uri);

        }


    }




    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {


        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String selection, @Nullable String[] selectionArgs) {

        final int match = sUriMatcher.match(uri);
        switch (match) {
            case M_TOTAL_EXPENSE_ID:

                selection = ExpenseContract.ExpenseEntry._ID + "=?";
                selectionArgs = new String[]{
                        String.valueOf(ContentUris.parseId(uri))
                };
                return updateMonthly_total_expense(uri, contentValues, selection, selectionArgs);

            default:
                throw new IllegalArgumentException("Updation is not supported for" + uri);
        }
    }



    //-----------------------------------------------------------------------------------------------------------------------------//

    private Uri insertExpense(Uri uri, ContentValues contentValues) {

     //   String title = contentValues.getAsString(ExpenseContract.ExpenseEntry.COLUMN_TITLE);
        Double price = contentValues.getAsDouble(ExpenseContract.ExpenseEntry.COLUMN_PVALUE);

        if (price != null && price < 0) {
            throw new IllegalArgumentException("Please Enter Valid Price");
        }

        //Get Writeable database
        SQLiteDatabase database = mhelper.getWritableDatabase();
        long id = database.insert(TABLE_NAME, null, contentValues);

        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row " + uri);
            return null;
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(uri, id);
    }




//----------------------------------------------------- INSERT MONTHLY EXPENSE TABLE-------------------------------------------//

    public Uri insert_monthlyExpense(Uri uri, ContentValues contentValues) {

        SQLiteDatabase database = mhelper.getWritableDatabase();
        long id = database.insert(ExpenseContract.ExpenseEntry.TABLE2, null, contentValues);

        if (id == -1) {
            Log.e(LOG_TAG, "Failed to insert row " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return ContentUris.withAppendedId(uri, id);
    }




    //Here int represents number of rows updated;
    private int updateMonthly_total_expense(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {

        if (contentValues.size() == 0) {
            return 0;
        }

        SQLiteDatabase database = mhelper.getWritableDatabase();
        int rowsUpdated = database.update(ExpenseContract.ExpenseEntry.TABLE2, contentValues, selection, selectionArgs);

        if (rowsUpdated != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return rowsUpdated;
    }


    //----------------------------------------------------------------------------------------------------------------------------//
}

// hello