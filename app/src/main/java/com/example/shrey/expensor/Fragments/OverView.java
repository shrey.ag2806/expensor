package com.example.shrey.expensor.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shrey.expensor.Bot.ChatBotActivity;
import com.example.shrey.expensor.ExpenseActivity;
import com.example.shrey.expensor.R;
import com.example.shrey.expensor.SettingsActivity;
import com.example.shrey.expensor.Utils.CalendarUtil;

import java.util.Calendar;
import java.util.TimeZone;

import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_MONTH;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_TEXPENSE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_YEAR;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.MCONTENT_URI;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry._ID;


public class OverView extends Fragment implements AdapterView.OnItemSelectedListener, LoaderManager.LoaderCallbacks<Cursor> {


    private static final int TOTAL_EXPENSE_LOADER = 0;

    private TextView price_textview;
    private ImageView currency_img;
    private String[] mnth = {"This month", "A"};
    private CardView addExpenseCard,chatBotCard;


    public OverView() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_over_view, container, false);

        price_textview = rootView.findViewById(R.id.price_textView);
        currency_img = rootView.findViewById(R.id.currency_img);
        currency_img.setImageResource(R.drawable.ruppe_icon);
        addExpenseCard = rootView.findViewById(R.id.add_expense_cardView);
        chatBotCard = rootView.findViewById(R.id.chat_bot_cardView);


        addExpenseCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ExpenseActivity.class));

            }
        });


        chatBotCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent chatIntent = new Intent(getContext(), ChatBotActivity.class);
                startActivity(chatIntent);


            }
        });

        editMonth();
        getTotalofMonth();

        setHasOptionsMenu(true);
        Toolbar mtoolbar = rootView.findViewById(R.id.overview_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(mtoolbar);
        mtoolbar.setTitle("Expensor");

        // getActivity().getSupportLoaderManager().initLoader(TOTAL_EXPENSE_LOADER, null, this);
        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.overview_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // return super.onOptionsItemSelected(item);


        switch (item.getItemId()) {

            case R.id.settings_option:
                startActivity(new Intent(getContext(), SettingsActivity.class));

                return true;
            default:
                return false;
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //TODO: Add currency change method;
        SharedPreferences sharedPreferences = getActivity().getApplicationContext().getSharedPreferences("MyPref", 0);
        String curr = sharedPreferences.getString("Currency", null);

        if (curr != null) {
            //
            if (curr.equals("rupees")) {
                currency_img.setImageResource(R.drawable.ruppe_icon);


            } else if (curr.equals("euro")) {
                currency_img.setImageResource(R.drawable.euro_icon);

            } else if (curr.equals("dollar")) {

                currency_img.setImageResource(R.drawable.dollar_icon);

            } else if (curr.equals("pound")) {

                currency_img.setImageResource(R.drawable.pound_icon);
            }


        }


    }


    @Override
    public void onResume() {
        super.onResume();

        getTotalofMonth();

    }


    private void editMonth() {
        Calendar c = Calendar.getInstance(TimeZone.getDefault());
        int currentMonth = c.get(Calendar.MONTH);

        if (currentMonth == 0) {
            mnth[1] = CalendarUtil.listOfMonths[11];
        } else {

            mnth[1] = CalendarUtil.listOfMonths[currentMonth - 1];
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {

        String[] projection = {
                _ID,
                COLUMN_TEXPENSE
        };
        String selection = COLUMN_MONTH + "=?" + " AND " + COLUMN_YEAR + "=?";
        String selectionArg[] = {CalendarUtil.getCurrentMonth(), CalendarUtil.getCurrentYear()};


        return new CursorLoader(getContext(), MCONTENT_URI,
                projection,
                selection,
                selectionArg,
                null
        );

    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {

        if (data.getCount() != 0) {
            data.moveToFirst();
            Double price = Double.parseDouble(data.getString(data.getColumnIndex(COLUMN_TEXPENSE)));
            price_textview.setText(String.valueOf(price));
        } else {

            price_textview.setText(R.string.zero_price);
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {


    }

    void getTotalofMonth() {

        String month = CalendarUtil.getCurrentMonth();

        //Log.d("OVERVIEW ACTIVITY","Month Inside function call " + name);
        String year = CalendarUtil.getCurrentYear();
        String projection[] = {_ID, COLUMN_TEXPENSE};
        String selection = COLUMN_MONTH + "=?" + " AND " + COLUMN_YEAR + "=?";
        String selectionArg[] = {month, year};

        Cursor cursor = getContext().getContentResolver().query(MCONTENT_URI, projection, selection, selectionArg, null);


        if (cursor.getCount() == 0) {
            Log.d("OVERVIEW- getTotal", "Cursor count is zero");
            price_textview.setText(R.string.ZeroValue);

        } else {

            cursor.moveToFirst();
            Log.d("OverView-getTotal", "Cursor Not null " + cursor);
            price_textview.setText(cursor.getString(cursor.getColumnIndex(COLUMN_TEXPENSE)));

        }

    }

}

