package com.example.shrey.expensor.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ExpenseDbHelper extends SQLiteOpenHelper {


    private static final String DATABASE_NAME = "expense.db";
    private static final int DATABASE_VERSION = 1;


    public ExpenseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_EXPENSE_TABLE = "CREATE TABLE " + ExpenseContract.ExpenseEntry.TABLE_NAME + " ("
                + ExpenseContract.ExpenseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ExpenseContract.ExpenseEntry.COLUMN_PVALUE + " TEXT NOT NULL, "
                + ExpenseContract.ExpenseEntry.COLUMN_TITLE + " TEXT NOT NULL, "
                + ExpenseContract.ExpenseEntry.COLUMN_DATE + " DATE NOT NULL, "
                + ExpenseContract.ExpenseEntry.COLUMN_NOTE + " TEXT );";

        //TABLE 2 for total expenses;
        String SQL_CREATE_MONTHLY_EXPENSE = "CREATE TABLE " + ExpenseContract.ExpenseEntry.TABLE2 + " ("
                + ExpenseContract.ExpenseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + ExpenseContract.ExpenseEntry.COLUMN_MONTH + " TEXT NOT NULL, "
                + ExpenseContract.ExpenseEntry.COLUMN_YEAR + " TEXT NOT NULL, "
                + ExpenseContract.ExpenseEntry.COLUMN_TEXPENSE + " TEXT NOT NULL); ";

        sqLiteDatabase.execSQL(SQL_CREATE_EXPENSE_TABLE);
        sqLiteDatabase.execSQL(SQL_CREATE_MONTHLY_EXPENSE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {


    }
}
