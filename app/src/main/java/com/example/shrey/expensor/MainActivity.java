
package com.example.shrey.expensor;


import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.shrey.expensor.Fragments.OverView;
import com.example.shrey.expensor.Fragments.history_fragment;


public class MainActivity extends AppCompatActivity{

    private BottomNavigationView btmview;
    private OverView ov_fragment;
    private history_fragment hFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btmview=findViewById(R.id.bottomNavigationView2);
        ov_fragment=new OverView();
        hFragment=new history_fragment();


        btmview.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()){

                    case R.id.overview_tab:
                        replaceFragment(ov_fragment);
                        return true;

                        case R.id.history_tab:
                          replaceFragment(hFragment);
                            return true;

                        default:
                            return false;
                }


            }
        });



    }

    @Override
    protected void onStart() {
        super.onStart();

        replaceFragment(ov_fragment);
    }

    public void replaceFragment(android.support.v4.app.Fragment f){
        android.support.v4.app.FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout_main,f);
        fragmentTransaction.commit();



    }


}
