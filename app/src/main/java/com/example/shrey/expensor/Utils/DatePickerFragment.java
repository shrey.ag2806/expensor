package com.example.shrey.expensor.Utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.shrey.expensor.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar cal=new GregorianCalendar(year,month,day);
        setDate(cal);

    }

    private void setDate(Calendar cal) {
            TextView edt=getActivity().findViewById(R.id.date_textview);
            DateFormat dateFormat=DateFormat.getDateInstance(DateFormat.MEDIUM);

            edt.setText(dateFormat.format(cal.getTime()));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c=Calendar.getInstance();
        int year=c.get(Calendar.YEAR);
        int month=c.get(Calendar.MONTH);
        int day=c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(getActivity(),this,year,month,day);

    }
}
