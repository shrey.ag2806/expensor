package com.example.shrey.expensor.Fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.shrey.expensor.Adapter.Exp_CursorAdapter;
import com.example.shrey.expensor.R;

import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_DATE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_PVALUE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_TITLE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.CONTENT_URI;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry._ID;


public class history_fragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    //TextView displayTextview;
    private ListView mListView;
    private Exp_CursorAdapter mcursorAdapter;

    android.support.v7.widget.Toolbar toolbar;

    //Identifier for expense data Loader=0;
    private static final int Expense_Loader = 0;

    public history_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_history_fragment, container, false);

        // displayTextview=rootView.findViewById(R.id.display_textView);

        toolbar = rootView.findViewById(R.id.toolbar3);
        toolbar.inflateMenu(R.menu.title_bar_menu);

        setHasOptionsMenu(true);

        mListView = rootView.findViewById(R.id.listview);


        mcursorAdapter = new Exp_CursorAdapter(getActivity(), null);
        mListView.setAdapter(mcursorAdapter);

        //Kick off the Loader
        getLoaderManager().initLoader(Expense_Loader, null, this);


        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        //      displaydatabaseinfo();
    }


    /*
        private void displaydatabaseinfo() {

            String[] projection={
                    ExpenseContract.ExpenseEntry._ID,
                    ExpenseContract.ExpenseEntry.COLUMN_TITLE,
                    ExpenseContract.ExpenseEntry.COLUMN_PVALUE,
                    ExpenseContract.ExpenseEntry.COLUMN_DATE,
                    ExpenseContract.ExpenseEntry.COLUMN_NOTE
            };


            //perform quer getContentResolver().
            Cursor cursor = getActivity().getApplicationContext().getContentResolver().query(ExpenseContract.ExpenseEntry.CONTENT_URI,
                        projection,
                    null,
                    null,
                    null

                    );

            try{
                displayTextview.setText("The expense table contains " +cursor.getCount() + " expenses\n\n");

                int idColumnIndex = cursor.getColumnIndex(ExpenseContract.ExpenseEntry._ID);
                int titleColumnIndex = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_TITLE);
                int priceColumnIndex = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_PVALUE);
                int dateColumnIndex = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_DATE);
                int noteColumnIndex = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_NOTE);


                while(cursor.moveToNext()){
                    int currentID=cursor.getInt(idColumnIndex);
                    String current_title=cursor.getString(titleColumnIndex);
                    Double current_price=cursor.getDouble(priceColumnIndex);
                    String current_date=cursor.getString(dateColumnIndex);
                    String current_note=cursor.getString(noteColumnIndex);

                    displayTextview.append(("\n" + currentID + " - " +
                            current_title + " - " +
                            current_price + " - " +
                            current_date + " - " +
                            current_note));

                }

            }finally {
                cursor.close();
            }
        }
    */
    @NonNull
    @Override
    public Loader<Cursor> onCreateLoader(int id, @Nullable Bundle args) {

        String[] projection = {
                _ID,
                COLUMN_TITLE,
                COLUMN_DATE,
                COLUMN_PVALUE
        };

        return new CursorLoader(getActivity(), CONTENT_URI,
                projection, null, null, COLUMN_DATE + " DESC" );


    }

    @Override
    public void onLoadFinished(@NonNull Loader<Cursor> loader, Cursor data) {
        mcursorAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<Cursor> loader) {
        mcursorAdapter.swapCursor(null);
    }
}

