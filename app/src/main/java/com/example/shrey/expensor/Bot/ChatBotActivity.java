package com.example.shrey.expensor.Bot;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shrey.expensor.MainActivity;
import com.example.shrey.expensor.R;
import com.example.shrey.expensor.Utils.CalendarUtil;
import com.example.shrey.expensor.data.QueryHelper;
import com.google.api.gax.core.FixedCredentialsProvider;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.dialogflow.v2beta1.DetectIntentResponse;
import com.google.cloud.dialogflow.v2beta1.QueryInput;
import com.google.cloud.dialogflow.v2beta1.QueryResult;
import com.google.cloud.dialogflow.v2beta1.SessionName;
import com.google.cloud.dialogflow.v2beta1.SessionsClient;
import com.google.cloud.dialogflow.v2beta1.SessionsSettings;
import com.google.cloud.dialogflow.v2beta1.TextInput;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;

import java.io.InputStream;
import java.util.Map;
import java.util.UUID;


public class ChatBotActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int USER = 1001;
    private static final int BOT = 1002;

    private String uuid = UUID.randomUUID().toString();
    private LinearLayout chatLayout;
    private EditText queryEditText;

    private SessionsClient sessionsClient;
    private SessionName session;
    ImageView sendBtn;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_bot);


        final ScrollView scrollView = findViewById(R.id.chatScrollView);
        scrollView.post(() -> scrollView.fullScroll(ScrollView.FOCUS_DOWN));


        chatLayout = findViewById(R.id.chatLayout);

        sendBtn = findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(this::sendMessage);


        queryEditText = findViewById(R.id.queryEditText);

        queryEditText.setOnKeyListener((view, keyCode, event) -> {

            if (event.getAction() == KeyEvent.ACTION_DOWN) {

                switch (keyCode) {

                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:
                        sendMessage(sendBtn);
                        return true;

                    default:
                        break;
                }

            }
            return false;
        });


        initializeChatBot();


    }


    private void initializeChatBot() {


        try {

            InputStream inputStream = getResources().openRawResource(R.raw.credential);
            GoogleCredentials googleCredentials = GoogleCredentials.fromStream(inputStream);

            String projectId = ((ServiceAccountCredentials) googleCredentials).getProjectId();

            SessionsSettings.Builder settingsBuilder = SessionsSettings.newBuilder();
            SessionsSettings sessionsSettings = settingsBuilder.setCredentialsProvider(FixedCredentialsProvider.create(googleCredentials)).build();
            sessionsClient = SessionsClient.create(sessionsSettings);
            session = SessionName.of(projectId, uuid);


        } catch (Exception e) {
            Log.d("ChatBotActivity", "Error occured in initializing chat bot");
            e.printStackTrace();
        }

    }

    private void sendMessage(View view) {

        String message = queryEditText.getText().toString();

        if (!message.trim().isEmpty()) {

            showTextView(message, USER);
            queryEditText.setText("");


            QueryInput queryInput = QueryInput.newBuilder().setText(TextInput.newBuilder().setText(message).setLanguageCode("en-US")).build();
            new RequestTask(ChatBotActivity.this, session, sessionsClient, queryInput).execute();

        }


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void callback(DetectIntentResponse detectIntentResponse) {


        if (detectIntentResponse != null) {
            Log.d("ChatBotActivity", "Inside");

            QueryResult result = detectIntentResponse.getQueryResult();


            String botreply = result.getFulfillmentText();
            //Log.d();
            showTextView(botreply, BOT);


            String intentName = "";

            try {
                intentName = result.getIntent().getDisplayName();

            } catch (NullPointerException e) {
                //showTextView();
            }

            if (intentName.equals("Check Balance")) {
                checkBalance(result);
            } else if (intentName.equals("Add Expense")) {
                addExpense(result);
            }

        } else {

            Log.d("ChatBotActivity", "Some error occured comm. issue");

        }


    }

    private void checkBalance(QueryResult result) {
        Struct struct = result.getParameters();
        Map<String, Value> map = struct.getFieldsMap();
        if (map.get("date-period") != null) {
            Struct dateStruct = struct.getFieldsOrDefault("date-period", null).getStructValue();
            Log.d("ChatBotActivity======", dateStruct.toString() + "");

            //TODO :: Check it datestruct is null or something. and modify this statement.
            String startDate = dateStruct.getFieldsOrDefault("startDate", null).getStringValue();
            Log.d("ChatBotActivity", startDate + " this is  start date");

            String month = CalendarUtil.getMonthFromString(startDate);
            String year = CalendarUtil.getYearFromString(startDate);
            showTextView(QueryHelper.getExpense(month, year, this), BOT);

        } else {
            showTextView(QueryHelper.getExpense(CalendarUtil.getCurrentMonth(),
                    CalendarUtil.getCurrentYear(), this), BOT);
        }
    }

    private void addExpense(QueryResult result) {

        Struct struct = result.getParameters();
        Log.d("ChatBotActivity==>>",struct.toString());

        Log.d("ChatBot","AfterTag");
        double amount = struct.getFieldsOrDefault("number",null).getNumberValue();


        Log.d("ChatBotActivity==>>",amount+"");

        if(amount!=0){


            sendBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(ChatBotActivity.this,"Success",Toast.LENGTH_SHORT).show();

                    String title = queryEditText.getText().toString();

                    if(!TextUtils.isEmpty(title.trim())){

                       String date= CalendarUtil.getDate();

                       QueryHelper.insertExpense(amount,title,date,getApplicationContext());

                       showTextView(title,USER);
                       showTextView("Your expense has been added",BOT);

                    }



                    queryEditText.setText("");
                    restoreSettings();

                }
            });
        }



    }

    private void restoreSettings() {
        sendBtn.setOnClickListener(this::sendMessage);
    }


    private void showTextView(String message, int type) {
        FrameLayout layout;

        switch (type) {
            case USER:
                layout = getUserLayout();
                break;
            case BOT:
                layout = getBotLayout();
                break;
            default:
                layout = getBotLayout();
                break;
        }
        layout.setFocusableInTouchMode(true);

        chatLayout.addView(layout); // move focus to text view to automatically make it scroll up if softfocus

        TextView tv = layout.findViewById(R.id.chatMsg);

        tv.setText(message);

        layout.requestFocus();

        queryEditText.requestFocus(); // change focus back to edit text to continue typing
    }


    FrameLayout getUserLayout() {
        LayoutInflater inflater = LayoutInflater.from(ChatBotActivity.this);
        return (FrameLayout) inflater.inflate(R.layout.user_message_layout, null);
    }


    FrameLayout getBotLayout() {
        LayoutInflater inflater = LayoutInflater.from(ChatBotActivity.this);
        return (FrameLayout) inflater.inflate(R.layout.bot_message_layout, null);
    }
}
