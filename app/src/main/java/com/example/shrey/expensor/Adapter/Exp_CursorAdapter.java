package com.example.shrey.expensor.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.example.shrey.expensor.R;
import com.example.shrey.expensor.data.ExpenseContract;

public class Exp_CursorAdapter extends CursorAdapter {
    public Exp_CursorAdapter(Context context, Cursor c) {
        super(context, c);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return LayoutInflater.from(context).inflate(R.layout.expense_list_item, viewGroup, false);


    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {

        //Finding views to be binded.

        TextView title = view.findViewById(R.id.expense_title);
        TextView date = view.findViewById(R.id.expense_date);
        TextView price = view.findViewById(R.id.expense_price);

        //Find the columns of expense we are interested in

        int title_idx = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_TITLE);
        int date_idx = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_DATE);
        int price_idx = cursor.getColumnIndex(ExpenseContract.ExpenseEntry.COLUMN_PVALUE);

        //read the data at these index;
        String c_title = cursor.getString(title_idx);
        String c_date = cursor.getString(date_idx);
        String c_price = cursor.getString(price_idx);

        //Update the textviews with these values;
        title.setText(c_title);
        date.setText(c_date);
        price.setText(c_price);
    }
}
