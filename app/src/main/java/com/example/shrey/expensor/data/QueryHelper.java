package com.example.shrey.expensor.data;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.example.shrey.expensor.Utils.CalendarUtil;

import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_DATE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_MONTH;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_NOTE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_PVALUE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_TEXPENSE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_TITLE;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.COLUMN_YEAR;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry.MCONTENT_URI;
import static com.example.shrey.expensor.data.ExpenseContract.ExpenseEntry._ID;

public class QueryHelper {

    static String TAG="Query-Helper";


    public static String getExpense(String month,String year,Context context){

        String projection[]={_ID,COLUMN_TEXPENSE};
        String selection = COLUMN_MONTH + "=?" + " AND " + COLUMN_YEAR + "=?";
        String selectionArg[] = {month, year};

        Cursor cursor = context.getContentResolver().query(MCONTENT_URI, projection, selection, selectionArg, null);

        if (cursor.getCount() == 0) {
            Log.d(TAG, "Cursor count is zero");
            return "You have no expenses";

        } else {

            cursor.moveToFirst();
            Log.d(TAG, "Cursor Not null " + cursor);

            String expense = cursor.getString(cursor.getColumnIndex(COLUMN_TEXPENSE));

            return "Your total expense for month "+ month +" is Rs."+expense;
        }

    }

    public static void updateTheTotal(Double price, long id,String date,Context context) {

        Uri in_upd_Uri = null;

        // TODO 1 : First query the database table 'total_monthly_expense' with the current month expense
        String year,month;

        if(date.equals("Today")){

            year = CalendarUtil.getCurrentYear();
            month = CalendarUtil.getCurrentMonth();



        }else{

            month = CalendarUtil.getMonthFromString2(date);
            year = CalendarUtil.getYearFromString2(date);

        }


        Log.d("Month",month+"");
        Log.d("Year",month+"");

        String projection[] = {_ID, COLUMN_MONTH, COLUMN_YEAR, COLUMN_TEXPENSE};
        String selection = COLUMN_MONTH + "=?" + " AND " + COLUMN_YEAR + "=?";
        String[] selectionArgs = {month, year};

        Cursor cursor = context.getContentResolver().query(MCONTENT_URI, projection, selection, selectionArgs, null);

        //If cursor does't have that entry then we have to insert it first in total expense table
        assert cursor != null;

        if (cursor.getCount() == 0) {

            Log.d("EXPENSE ACTIVITY", "Cursor count is zero for Total monthly Expense so add new");

            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_MONTH, month);
            contentValues.put(COLUMN_YEAR, year);
            contentValues.put(COLUMN_TEXPENSE, String.valueOf(price));

            in_upd_Uri = context.getContentResolver().insert(MCONTENT_URI, contentValues);


        } else {

            cursor.moveToFirst();
            long i_d = cursor.getLong(cursor.getColumnIndex(_ID));
            in_upd_Uri = ContentUris.withAppendedId(MCONTENT_URI, i_d);


            int tExpenseIndex = (cursor.getColumnIndex("total_expense"));
            Double currentPrice = Double.parseDouble(cursor.getString(tExpenseIndex));
            Double newP = currentPrice + price;

            Log.d("Updated Price:", newP + "");

            // TODO 3 : Update the table with new expense total expense of the month.

            Log.d("IN UPD URI:", in_upd_Uri + "");

            ContentValues contentValues = new ContentValues();
            contentValues.put(COLUMN_MONTH, month);
            contentValues.put(COLUMN_YEAR, year);
            contentValues.put(COLUMN_TEXPENSE, String.valueOf(newP));

            String selection2 = COLUMN_MONTH + "=?" + " AND " + COLUMN_YEAR + "=?";
            String[] selectionArgs2 = {month, year};

            //Log.d("MONTH AND YEAR", "Month: " + month + " YEAR: " + year);

            context.getContentResolver().update(in_upd_Uri, contentValues, selection2, selectionArgs2);


        }

        cursor.close();

    }


    public static void insertExpense(double amount, String title, String date,Context context) {

        ContentValues contentValues =new ContentValues();
        contentValues.put(COLUMN_PVALUE,String.valueOf(amount));
        contentValues.put(COLUMN_TITLE,title);
        contentValues.put(COLUMN_DATE,date);
        contentValues.put(COLUMN_NOTE,"");

        Uri uri = context.getContentResolver().insert(ExpenseContract.ExpenseEntry.CONTENT_URI, contentValues);

        long id = ContentUris.parseId(uri);


        updateTheTotal(amount,id,date,context);

        //Toast.makeText(this, "Expense has been added", Toast.LENGTH_LONG).show();


    }
}
