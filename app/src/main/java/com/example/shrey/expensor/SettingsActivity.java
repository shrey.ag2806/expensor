package com.example.shrey.expensor;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.shrey.expensor.DialogFragments.CurrencyDialog;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    private ListView mlistview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        mlistview=findViewById(R.id.settings_listView);

        List<String> list=  new ArrayList<>();
        list.add("Currency");
        list.add("Theme");

        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,list);
        mlistview.setAdapter(arrayAdapter);

        View headerView=getLayoutInflater().inflate(R.layout.list_view_header1,null);

        mlistview.addHeaderView(headerView);

        mlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String selectedItem=(String)adapterView.getItemAtPosition(i);

                switch (selectedItem){
                    case "Currency":
                        currencyClickMethod();


                }

            }
        });


    }

    private void currencyClickMethod() {

        FragmentManager fm=getSupportFragmentManager();
        DialogFragment dialogFragment = new CurrencyDialog();

        dialogFragment.show(fm,"CurrencyDialog");







    }
}
