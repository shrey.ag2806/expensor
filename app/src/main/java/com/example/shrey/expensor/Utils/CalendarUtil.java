package com.example.shrey.expensor.Utils;

import java.util.Calendar;
import java.util.TimeZone;

public class CalendarUtil {

    public static  final  String[] listOfMonths = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};


    public CalendarUtil(){


    }


    public static String getCurrentMonth (){
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int currentMonth = calendar.get(Calendar.MONTH);
        return  listOfMonths[currentMonth];

    }

    public static String getCurrentYear(){
        Calendar calendar= Calendar.getInstance(TimeZone.getDefault());
        int currentYear = calendar.get(Calendar.YEAR);
        return String.valueOf(currentYear);
    }

    public static String getMonthFromString(String str){

        String month = str.substring(5,7);

        int i =Integer.parseInt(month);
        return listOfMonths[i-1];

    }

    public static String getYearFromString(String s) {

        return s.substring(0,4);
    }

    public static String getMonthFromString2(String date) {
        String d = date.substring(3,6);
        String ans="";
        for (String listOfMonth : listOfMonths) {

            if (d.equals(listOfMonth.substring(0, 3))) {
                    ans=listOfMonth;
                    break;
            }

        }
        return ans;
    }

    public static String getYearFromString2(String date) {

        return date.substring(7);

    }

    public static int getYearInt(String date){

        return  Integer.valueOf(date.substring(7,11));
    }

    public static int getDateFromString2(String date){
        return Integer.parseInt(date.substring(0,2));

    }

    public static int getMonthInt(String date){
        String d = date.substring(3,6);
        int ans=0;

        for(int i=0;i<listOfMonths.length;i++){

            if(listOfMonths[i].substring(0,3).equals(d)){
                ans=i;
                break;
            }

        }
        return ans;
    }
    public static  String getDate(){

        Calendar c = Calendar.getInstance(TimeZone.getDefault());
        int date =c.get(Calendar.DATE);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);
        String mon=listOfMonths[month].substring(0,3);

        return date+"-"+mon+"-"+year;


    }





}
