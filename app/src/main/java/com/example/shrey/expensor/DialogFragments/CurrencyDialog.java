package com.example.shrey.expensor.DialogFragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.shrey.expensor.R;

public class CurrencyDialog extends DialogFragment {

    ImageView dollar,euro,pound,rupees;
    Button ok,cancel;
    String currency="";




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.currency_dialog,container,false);

        dollar=v.findViewById(R.id.currency_dollar);
        euro=v.findViewById(R.id.currency_euro);
        rupees=v.findViewById(R.id.currency_rupees);
        pound=v.findViewById(R.id.currency_pound);


        dollar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                currency="dollar";
                dollar.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                rupees.setBackground(null);
                euro.setBackground(null);
                pound.setBackground(null);

            }
        });

        rupees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency="rupees";
                rupees.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                dollar.setBackground(null);
                euro.setBackground(null);
                pound.setBackground(null);

            }
        });


        pound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency="pound";
                pound.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                rupees.setBackground(null);
                euro.setBackground(null);
                dollar.setBackground(null);

            }
        });

        euro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currency="euro";
                euro.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                rupees.setBackground(null);
                dollar.setBackground(null);
                pound.setBackground(null);

            }
        });

        ok=v.findViewById(R.id.currency_ok);
        cancel=v.findViewById(R.id.currency_cancel);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //on clicking ok :
                 // Triggering a method to change currencey;
                //check if currency string is changed or not;
                setCurrency();

                dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


        return v;



    }


    void setCurrency(){
        SharedPreferences sharedPreferences=getActivity().getApplicationContext().getSharedPreferences("MyPref",0);

        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString("Currency",currency);
        editor.commit();

    }

}
