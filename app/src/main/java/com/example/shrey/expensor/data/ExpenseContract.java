package com.example.shrey.expensor.data;

import android.net.Uri;
import android.net.wifi.aware.PublishConfig;
import android.provider.BaseColumns;

public final class ExpenseContract {

    //To prevent someone from accidentally instantiating the contract class
    private ExpenseContract() {

    }

    public static final String CONTENT_AUTHORITY = "com.example.shrey.expensor";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    //possible paths

    public static final String PATH_EXPENSE = "expenses";
    public static final String PATH_TOTAL_EXPENSE = "monthly_total";


    public static final class ExpenseEntry implements BaseColumns {

        //uri
        public final static Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_EXPENSE);
        public final static Uri MCONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_TOTAL_EXPENSE);


        public final static String TABLE_NAME = "expenses";
        public final static String _ID = BaseColumns._ID;
        public final static String COLUMN_TITLE = "title";
        public final static String COLUMN_PVALUE = "price";
        public final static String COLUMN_DATE = "date";
        public final static String COLUMN_NOTE = "note";

        //Table 2 with id and total expense of current month.

        public final static String TABLE2 = "monthly_total";
        public final static String COLUMN_MONTH = "month";
        public final static String COLUMN_YEAR = "year";
        public final static String COLUMN_TEXPENSE = "total_expense";


    }

}
